PATH=$PATH:/usr/local/bin
export PATH

cd /home/spot1/transportes/transportes/spiders/

scrapy runspider cocha_com.py
scrapy runspider turbus_cl.py
scrapy runspider despegar_cl.py
#scrapy runspider json_despegar.py

echo 'Archivos csv generados por script del mismo nombre sin la palabra output_ y extension .csv' >> errores.csv
for entry in $(ls output_json*)
do
   if [ -s ${entry} ];then
       echo archivo correcto: ${entry}
       #Enviamos los datos a S3
       aws s3 cp ${entry} s3://spot-backup/spot-transportes/json/`date +%Y%m%d_%H`_d/
       aws s3 mv ${entry} s3://spot-transportes/json/`date +%Y%m%d_%H`_d/
       rm ${entry}

   else
       echo archivo vacío : ${entry}
       echo  ${entry} >> errores.csv
   fi
done

scrapy runspider json_despegar.py

for entry in $(ls output_*)
do
   if [ -s ${entry} ];then
       echo archivo correcto: ${entry}
       #Enviamos los datos a S3
       aws s3 cp ${entry} s3://spot-backup/spot-transportes/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d/
       aws s3 mv ${entry} s3://spot-transportes/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d/
       rm ${entry}

   else
       echo archivo vacío : ${entry}
       echo  ${entry} >> errores.csv
   fi
done

#Se sube archivo general  output.csv
#aws s3 mv output.csv s3://spot-transportes/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d.csv

#Se sube archivo errores
aws s3 cp errores.csv s3://spot-backup/spot-transportes/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv
aws s3 mv errores.csv s3://spot-transportes/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv

#Se eliminan archivos .pyc y .csv
cd /home/spot1/transportes
find . -name \*.pyc -delete
find . -name \*.csv -delete

cd /home/spot1/cronlog
aws s3 cp bash_`date +%Y%m%d_%H`_d.log  s3://spot-backup/spot-transportes/descargasScrapy/bash_run/bash_`date +%Y%m%d_%H`_d.log
aws s3 mv bash_`date +%Y%m%d_%H`_d.log  s3://spot-transportes/descargasScrapy/bash_run/bash_`date +%Y%m%d_%H`_d.log
find . -name \*.log -delete
