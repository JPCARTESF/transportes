# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html


import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, Compose, Join, MapCompose, TakeFirst
from w3lib.html import remove_tags
import time
import re

class TransportesItem(scrapy.Item):
	empresa  = scrapy.Field()
	servicio  = scrapy.Field()
	terminal_salida = scrapy.Field()
	terminal_llegada = scrapy.Field()
	hora_salida  = scrapy.Field()
	hora_llegada = scrapy.Field()
	fecha_salida = scrapy.Field()
	fecha_llegada = scrapy.Field()
	clase = scrapy.Field()
	precio =scrapy.Field()
	ciudad_salida = scrapy.Field()
	ciudad_llegada = scrapy.Field()
	#datos estatico 
	hora_extraccion = scrapy.Field()
	fecha_extraccion = scrapy.Field()
	fuente = scrapy.Field()
	url  = scrapy.Field()

	#precio_primerpiso = scrapy.Field()
	#precio_segundopiso = scrapy.Field()
	id_busqueda = scrapy.Field()
	nombre_destino = scrapy.Field()
	codigo_destino = scrapy.Field()
	ciudad_destino = scrapy.Field()
	pais_destino = scrapy.Field()
	ciudad_origen = scrapy.Field()
	pais_destino = scrapy.Field()
	nombre_origen = scrapy.Field()
	codigo_origen = scrapy.Field()
	ratio_clp = scrapy.Field()
	ratio_usd = scrapy.Field()
	aeropuerto_ida = scrapy.Field()
	fecha_hora_ida = scrapy.Field()
	aeropuerto_vuelta = scrapy.Field()
	fecha_hora_vuelta = scrapy.Field()
	codigo_areolinea = scrapy.Field()
	id_vuelo = scrapy.Field()
	tipo_cabina = scrapy.Field()
	total_duracion_vuelo_ida = scrapy.Field()
	tipo_precio_final = scrapy.Field()
	precio_bruto = scrapy.Field()
	imp_y_tasas = scrapy.Field()
	cargos = scrapy.Field()
	precio_total = scrapy.Field()
	hora_extraccion = scrapy.Field()
	fecha_extraccion = scrapy.Field()
	fuente = scrapy.Field()
	url = scrapy.Field()

	fecha_hora_ida_salida = scrapy.Field()
	fecha_hora_ida_llegada = scrapy.Field()
	fecha_hora_vuelta_salida = scrapy.Field()
	fecha_hora_vuelta_llegada = scrapy.Field()
	total_duracion_vuelo_vuelta = scrapy.Field()
	aviones_escala = scrapy.Field()
	numero_escalas = scrapy.Field()


	json_data = scrapy.Field()
	url_json = scrapy.Field()
	url_view = scrapy.Field()
	iata_origen = scrapy.Field()
	iata_destino = scrapy.Field()
	fecha_checkin =  scrapy.Field()
	fecha_checkout = scrapy.Field()

	#VIAJES COCHA
	id_viaje_ida = scrapy.Field()
	iata_origen = scrapy.Field()
	iata_llegada = scrapy.Field()
	fecha_ida = scrapy.Field()
	fecha_llegada = scrapy.Field()
	aerolinea_ida = scrapy.Field()
	ciudad_origen = scrapy.Field()
	pais_origen = scrapy.Field()
	ciudad_destino = scrapy.Field()
	pais_destino = scrapy.Field()
	id_viaje_regreso = scrapy.Field()
	iata_origen_regreso = scrapy.Field()
	iata_llegada_regreso = scrapy.Field()
	fecha_ida_regreso = scrapy.Field()
	fecha_llegada_regreso = scrapy.Field()
	aerolinea_regreso = scrapy.Field()
	precio_base_usd = scrapy.Field()
	precio_taxes_usd = scrapy.Field()
	precio_fee_usd = scrapy.Field()
	precio_total_usd = scrapy.Field()
	precio_base_clp = scrapy.Field()
	precio_taxes_clp = scrapy.Field()
	precio_fee_clp = scrapy.Field()
	precio_total_clp = scrapy.Field()
	hora_extraccion = scrapy.Field()
	fecha_extraccion = scrapy.Field()
	fuente = scrapy.Field()
	url = scrapy.Field()   




class TransportesLoader(ItemLoader):
	default_item_class = TransportesItem

	def strip_dashes(value):
		if value:
			return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','').strip()
	def strip_others(value):
		if value:
			return value.replace('\t','').replace('\n','').replace('\r','')
	def clear_city(value):
		if value:
			patron = re.compile("(\w+)((\s)?(\w+))+")
			s = patron.search(value)
			if s:
				return s.group()
			else:
				return value



	ciudad_salida_in  =  Compose(TakeFirst(),clear_city)
	ciudad_llegada_in  = Compose(TakeFirst(),clear_city)

	default = TakeFirst()
	empresa_out = default
	servicio_out  = default
	terminal_salida_out = default
	terminal_llegada_out = default
	hora_salida_out  = default
	hora_llegada_out = default
	fecha_salida_out = default
	fecha_llegada_out = default
	clase_out  = default
	precio_out = default
	ciudad_salida_out = default
	ciudad_llegada_out = default

	hora_extraccion_out = default
	fecha_extraccion_out = default
	fuente_out = default
	url_out = default

class DespegarLoader(ItemLoader):
	default_item_class = TransportesItem
	def strip_dashes(value):
		if value:
			return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','').strip()



	pais_destino_in  = Compose(Join(),strip_dashes)

	id_busqueda_out = TakeFirst()
	nombre_destino_out = TakeFirst()
	codigo_destino_out = TakeFirst()
	ciudad_destino_out = TakeFirst()
	pais_destino_out = TakeFirst()
	ciudad_origen_out = TakeFirst()
	pais_destino_out = TakeFirst()
	nombre_origen_out = TakeFirst()
	codigo_origen_out = TakeFirst()
	ratio_clp_out = TakeFirst()
	ratio_usd_out = TakeFirst()

	aeropuerto_ida_out = TakeFirst()
	fecha_hora_ida_out = TakeFirst()
	aeropuerto_vuelta_out = TakeFirst()
	fecha_hora_vuelta_out = TakeFirst()
	codigo_areolinea_out = TakeFirst()
	id_vuelo_out = TakeFirst()
	tipo_cabina_out = TakeFirst()
	total_duracion_vuelo_ida_out = TakeFirst()
	tipo_precio_final_out = TakeFirst()
	precio_bruto_out = TakeFirst()
	imp_y_tasas_out = TakeFirst()
	cargos_out = TakeFirst()
	precio_total_out = TakeFirst()
	hora_extraccion_out = TakeFirst()
	fecha_extraccion_out = TakeFirst()
	fuente_out = TakeFirst()
	url_out = TakeFirst()

	fecha_hora_ida_salida_out = TakeFirst()
	fecha_hora_ida_llegada_out = TakeFirst()
	fecha_hora_vuelta_salida_out = TakeFirst()
	fecha_hora_vuelta_llegada_out = TakeFirst()
	total_duracion_vuelo_vuelta_out = TakeFirst()
	aviones_escala_out = TakeFirst()
	numero_escalas_out = TakeFirst()

class json_loader(ItemLoader):
	default_item_class= TransportesItem 

	#Contiene el json
	json_data_out = TakeFirst()

	iata_origen_out = TakeFirst()
	iata_destino_out = TakeFirst()
	fecha_checkin_out = TakeFirst()
	fecha_checkout_out =TakeFirst()

	fecha_extraccion_out = TakeFirst()
	hora_extraccion_out = TakeFirst()
	url_json_out = TakeFirst()
	url_view_out = TakeFirst()
	fuente_out =TakeFirst()

class cochaLoader(ItemLoader):
	default_item_class= TransportesItem 

	id_viaje_ida_out = TakeFirst()
	iata_origen_out = TakeFirst()
	iata_llegada_out = TakeFirst()
	fecha_ida_out = TakeFirst()
	fecha_llegada_out = TakeFirst()
	aerolinea_ida_out = TakeFirst()
	ciudad_origen_out = TakeFirst()
	pais_origen_out = TakeFirst()
	ciudad_destino_out = TakeFirst()
	pais_destino_out = TakeFirst()
	id_viaje_regreso_out = TakeFirst()
	iata_origen_regreso_out = TakeFirst()
	iata_llegada_regreso_out = TakeFirst()
	fecha_ida_regreso_out = TakeFirst()
	fecha_llegada_regreso_out = TakeFirst()
	aerolinea_regreso_out = TakeFirst()
	precio_base_usd_out = TakeFirst()
	precio_taxes_usd_out = TakeFirst()
	precio_fee_usd_out = TakeFirst()
	precio_total_usd_out = TakeFirst()
	precio_base_clp_out = TakeFirst()
	precio_taxes_clp_out = TakeFirst()
	precio_fee_clp_out = TakeFirst()
	precio_total_clp_out = TakeFirst()
	hora_extraccion_out = TakeFirst()
	fecha_extraccion_out = TakeFirst()
	fuente_out = TakeFirst()
	url_out = TakeFirst()  





