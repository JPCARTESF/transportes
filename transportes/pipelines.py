# -*- coding: utf-8 -*-
from transportes import items
from transportes import settings
from transportes.items import *
import csv

class TransportesPipeline(object):
	def __init__(self):
		self.file_name = {} 
		self.keys_turbus = {'empresa','servicio','terminal_salida','terminal_llegada','hora_salida','hora_llegada','fecha_salida','fecha_llegada','clase','precio','ciudad_salida','ciudad_llegada','fuente','url','fecha_extraccion','hora_extraccion'}
		self.keys_despegar = {'id_busqueda','nombre_destino','codigo_destino','ciudad_destino','pais_destino','ciudad_origen','pais_destino','nombre_origen','codigo_origen','ratio_clp','ratio_usd','precio_bruto','imp_y_tasas','cargos','precio_total','tipo_precio_final','aviones_escala','numero_escalas','aeropuerto_ida','fecha_hora_ida_salida','fecha_hora_ida_llegada','total_duracion_vuelo_ida','aeropuerto_vuelta','fecha_hora_vuelta_salida','fecha_hora_vuelta_llegada','total_duracion_vuelo_vuelta','hora_extraccion','fecha_extraccion','fuente','url'}
		self.keys_cocha = {'ciudad_origen','pais_origen','ciudad_destino','pais_destino','id_viaje_ida','iata_origen','iata_llegada','fecha_ida','fecha_llegada','aerolinea_ida','id_viaje_regreso','iata_origen_regreso','iata_llegada_regreso','fecha_ida_regreso','fecha_llegada_regreso','aerolinea_regreso','precio_base_usd','precio_taxes_usd','precio_fee_usd','precio_total_usd','precio_base_clp','precio_taxes_clp','precio_fee_clp','precio_total_clp','hora_extraccion','fecha_extraccion','fuente','url'}
	def open_spider(self, spider):
		self.file_name=csv.writer(open('output_'+spider.name+'.csv','w'))
		if spider.name.find('turbus')!=-1:
			self.file_name.writerow(['empresa','servicio','terminal_salida','terminal_llegada','hora_salida','hora_llegada','fecha_salida','fecha_llegada','clase','precio','ciudad_salida','ciudad_llegada','fuente','url','fecha_extraccion','hora_extraccion'])
		elif spider.name.find('despegar_cl')!=-1:
			self.file_name.writerow(['id_busqueda','codigo_destino','ciudad_destino','pais_destino','ciudad_origen','pais_destino','codigo_origen','ratio_clp','ratio_usd','precio_bruto','imp_y_tasas','cargos','precio_total','tipo_precio_final','aviones_escala','numero_escalas','aeropuerto_ida','fecha_hora_ida_salida','fecha_hora_ida_llegada','total_duracion_vuelo_ida','aeropuerto_vuelta','fecha_hora_vuelta_salida','fecha_hora_vuelta_llegada','total_duracion_vuelo_vuelta','hora_extraccion','fecha_extraccion','fuente','url'])
		elif spider.name.find('json_despegar')!=-1:
			self.file_name.writerow(['json_data','iata_origen','iata_destino','fecha_checkin','fecha_checkout','url_json','url_view','fuente','fecha_extraccion','hora_extraccion'])
		elif spider.name.find('cocha_com')!=-1:
			self.file_name.writerow(['ciudad_origen','pais_origen','ciudad_destino','pais_destino','id_viaje_ida','iata_origen','iata_llegada','fecha_ida','fecha_llegada','aerolinea_ida','id_viaje_regreso','iata_origen_regreso','iata_llegada_regreso','fecha_ida_regreso','fecha_llegada_regreso','aerolinea_regreso','precio_base_usd','precio_taxes_usd','precio_fee_usd','precio_total_usd','precio_base_clp','precio_taxes_clp','precio_fee_clp','precio_total_clp','hora_extraccion','fecha_extraccion','fuente','url'])
	def process_item(self, item, spider):
		
		if spider.name.find('turbus')!=-1:
			for key in self.keys_turbus:
				item.setdefault(key,'NA')

			self.file_name.writerow([
					item['empresa'],
					item['servicio'],
					item['terminal_salida'],
					item['terminal_llegada'],
					item['hora_salida'],
					item['hora_llegada'],
					item['fecha_salida'],
					item['fecha_llegada'],
					item['clase'],
					item['precio'],
					item['ciudad_salida'],
					item['ciudad_llegada'],
					item['fuente'],
					item['url'],
					item['fecha_extraccion'],
					item['hora_extraccion'],

				])
			return item

		elif spider.name.find('despegar_cl')!=-1:
			for key in self.keys_despegar:
				item.setdefault(key,'NA')

			self.file_name.writerow([
					item['id_busqueda'],
					#item['nombre_destino'],
					item['codigo_destino'],
					item['ciudad_destino'],
					item['pais_destino'],
					item['ciudad_origen'],
					item['pais_destino'],
					#item['nombre_origen'],
					item['codigo_origen'],
					item['ratio_clp'],
					item['ratio_usd'],
					item['precio_bruto'],
					item['imp_y_tasas'],
					item['cargos'],
					item['precio_total'],
					item['tipo_precio_final'],
					item['aviones_escala'],
					item['numero_escalas'],
					item['aeropuerto_ida'],
					item['fecha_hora_ida_salida'],
					item['fecha_hora_ida_llegada'],
					item['total_duracion_vuelo_ida'],
					item['aeropuerto_vuelta'],
					item['fecha_hora_vuelta_salida'],
					item['fecha_hora_vuelta_llegada'],
					item['total_duracion_vuelo_vuelta'],
					item['hora_extraccion'],
					item['fecha_extraccion'],
					item['fuente'],
					item['url']
				])
			return item
		elif spider.name.find('json_despegar')!=-1:
			for key in self.keys_despegar:
				item.setdefault(key,'NA')

			self.file_name.writerow([
								item['json_data'],
								item['iata_origen'],
								item['iata_destino'],
								item['fecha_checkin'],
								item['fecha_checkout'],
								item['url_json'],
								item['url_view'],
								item['fuente'],
								item['fecha_extraccion'],
								item['hora_extraccion'],
								])
			return item

		elif spider.name.find('cocha_com')!=-1:
			for key in self.keys_cocha:
				item.setdefault(key,'NA')

			self.file_name.writerow([
								item['ciudad_origen'],
								item['pais_origen'],
								item['ciudad_destino'],
								item['pais_destino'],
								item['id_viaje_ida'],
								item['iata_origen'],
								item['iata_llegada'],
								item['fecha_ida'],
								item['fecha_llegada'],
								item['aerolinea_ida'],
								item['id_viaje_regreso'],
								item['iata_origen_regreso'],
								item['iata_llegada_regreso'],
								item['fecha_ida_regreso'],
								item['fecha_llegada_regreso'],
								item['aerolinea_regreso'],
								item['precio_base_usd'],
								item['precio_taxes_usd'],
								item['precio_fee_usd'],
								item['precio_total_usd'],
								item['precio_base_clp'],
								item['precio_taxes_clp'],
								item['precio_fee_clp'],
								item['precio_total_clp'],
								item['hora_extraccion'],
								item['fecha_extraccion'],
								item['fuente'],
								item['url'],
								])
			return item






				






