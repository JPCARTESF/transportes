# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import FormRequest, Request
from transportes.items import TransportesLoader
from datetime import datetime, date, time, timedelta
import time



class TurbusClSpider(scrapy.Spider):
    name = 'turbus_cl'
    allowed_domains = ['turbus.cl']
    start_urls = ['https://www.turbus.cl/wtbus/indexCompra.jsf']
  
    def parse(self, response):
        ciudades = response.xpath('//*[@id="j_id_id297:cmbCiudadOrigen"]//script//text()').re_first('Algarrobo.*\]')
        hoy =  date.today()

        #Se obtienen fechas para obtener los pasajes del proximo día
        mañana = hoy + timedelta(days=1)
            
        hoy_str = str(hoy).replace('-','/')
        hoy_str = hoy_str.split('/')[2]+'/'+hoy_str.split('/')[1]+'/'+hoy_str.split('/')[0]
        
        mañana_str = str(mañana).replace('-','/')
        mañana_str = mañana_str.split('/')[2]+'/'+mañana_str.split('/')[1]+'/'+mañana_str.split('/')[0]

        periodo_hoy = hoy_str.split('/')[1] + '/'+ str(hoy_str).split('/')[2]
        periodo_mañana = mañana_str.split('/')[1] + '/'+mañana_str.split('/')[2] 
        
        #Se envía petición para obtener pasaje - IDA
        for ciudad_destino in ciudades.replace("'","").replace("]",'').split(','):
            for ciudad_origen in ciudades.replace("'","").replace("]",'').split(','):
                if ciudad_origen != ciudad_destino:
                    yield Request(
                                   url = 'https://www.turbus.cl/wtbus/indexCompra.jsf'
                                  ,dont_filter = True
                                  ,callback = self.parse_other 
                                  ,meta ={'ciudad_destino':ciudad_destino
                                          ,'ciudad_origen':ciudad_origen
                                          ,'hoy':hoy_str
                                          ,'mañana':mañana_str
                                          ,'periodo_hoy':periodo_hoy
                                          ,'periodo_mañana':periodo_mañana
                                          }
                                )

    def parse_other(self,response):
        token = response.xpath('//*[@id="javax.faces.ViewState"]//@value').extract_first()
        #print(response.meta['periodo_hoy'] +' '+response.meta['periodo_mañana']+' '+response.meta['hoy'] + ' ' +response.meta['mañana'] )
        yield FormRequest(
                     #response.body
                     url='https://www.turbus.cl/wtbus/indexCompra.jsf'
                    ,formdata={'AJAXREQUEST':'j_id_id9'
                                ,'j_id_id109:radioSoloIda':'IDA'
                                ,'j_id_id109:cmbCiudadOrigenV2':response.meta['ciudad_origen']
                                ,'j_id_id109:j_id_id146_selection':''
                                ,'j_id_id109:calIdaV2InputDate':response.meta['mañana']
                                ,'j_id_id109:calIdaV2InputCurrentDate':response.meta['periodo_mañana']
                                ,'j_id_id109:horarioIdaV2':'TODOS'
                                ,'j_id_id109:cmbCiudadDestinoV2':response.meta['ciudad_destino']
                                #,'j_id_id109:j_id_id186_selection':''
                                ,'j_id_id109:calVueltaV2InputDate':response.meta['hoy']
                                ,'j_id_id109:calVueltaV2InputCurrentDate':response.meta['periodo_hoy']
                                ,'j_id_id109:horarioRegresoV2':'TODOS'
                                ,'j_id_id109:cantidadPasajesV2':'1'
                                ,'j_id_id109':'j_id_id109'
                                #,'autoScroll':''
                                ,'javax.faces.ViewState':token
                                ,'j_id_id109:botonContinuarV2':'j_id_id109:botonContinuarV2'
                            }
                     ,callback=self.parse_data
                    ,meta ={'ciudad_destino':response.meta['ciudad_destino']
                            ,'ciudad_origen':response.meta['ciudad_origen']
                            }
                )

    
    def parse_data(self,response):
        #print( +' - '+ response.meta['ciudad_destino']+ ' !')
        loader = TransportesLoader()
        for pasajes in  response.xpath("//table[@class='rich-table table']//tr"):
            pasaje = pasajes.xpath('./td//text()').extract()
            if pasaje:
                loader = TransportesLoader()
                loader.add_value(  "fecha_salida"     , pasaje[0] )
                loader.add_value(  "hora_salida"      , pasaje[1] )
                loader.add_value(  "terminal_salida"  , pasaje[2] ) 
                loader.add_value(  "terminal_llegada" , pasaje[3] ) 
                loader.add_value(  "fecha_llegada"    , pasaje[4] ) 
                loader.add_value(  "hora_llegada"     , pasaje[5] ) 
                loader.add_value(  "precio"           , pasaje[6] )
                loader.add_value(  "clase"            , pasaje[7] )
                loader.add_value(  "ciudad_salida"    ,response.meta['ciudad_origen'])
                loader.add_value(  "ciudad_llegada"   ,response.meta['ciudad_destino'])
                loader.add_value(  "empresa"          ,'TURBUS')
                loader.add_value(  "servicio"         ,'IDA')
                loader.add_value(  "hora_extraccion"  ,time.strftime("%H:%M:%S"))
                loader.add_value(  "fecha_extraccion" ,time.strftime("%d/%m/%Y"))
                loader.add_value(  "fuente"           ,"www.turbus.cl")
                loader.add_value(  "url"              , response.url)
                yield(loader.load_item())


